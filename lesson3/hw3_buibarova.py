# Завдання 1. Напишіть цикл, який буде вимагати від користувача ввести слово, в якому є буква "о" (враховуються як великі так і маленькі).
# Цикл не повинен завершитися, якщо користувач ввів слово без букви "о".

while True:
    users_word = input("Введіть слово в якому є буква о (велика чи мала): ")
    if ("о" in users_word) or ("О" in users_word):
        print("Дякую!")
        break
    else:
        print("Введено непідходяще слово, спробуйте ще.")

# Завдання 2. Є list з даними lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишіть код, який свормує новий list (наприклад lst2), який містить лише змінні типу стрінг, які присутні в lst1.
# Зауважте, що lst1 не є статичним і може формуватися динамічно від запуску до запуску.

lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
lst2 = []
for element in lst1:
    if type(element) == str:
        lst2.append(element)
try:
    1 / len(lst2)
except ZeroDivisionError:
    print("У lst1 немає елементів типу String!")

print("lst2 = ", lst2)

# Завдання 3. Є стрінг з певним текстом (можна скористатися input або константою).
# Напишіть код, який визначить кількість слів в цьому тексті, які закінчуються на "о" (враховуються як великі так і маленькі).

input_string = input("Задайте строку: ")
# тестова строка для інпуту:
# Дано числа a і b (a < b). Виведіть на екран суму всіх "натуральних" чисел a:b (включно), які є 'кратними' середньому арифметичному цього проміжку! GL, HF?!...

separators_list = [".", "?", "!", ";", ":", "—", "\"", "\'", "(", ")", ","]
separated_string = input_string
for separator in separators_list:
    separated_string = separated_string.replace(separator, "")

input_list = separated_string.split()
#print(input_list)

o_counter = 0
for element in input_list:
    if element.endswith("о") or element.endswith("О"):
        o_counter += 1
print("Кількість слів, які закінчуються на 'о': ", o_counter)
