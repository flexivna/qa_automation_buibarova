# Завдання1 Є list довільних чисел, наприклад [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44].
# Напишіть код, який видалить (не створить новий, а саме видалить!) з нього всі числа, які менше 21 і більше 74.
#
# задайте Dict
input_lst = [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44, 66]

# задайте межі
lower_limit = 21
upper_limit = 74
iterator = 0

while iterator < len(input_lst):
    if input_lst[iterator] < lower_limit or input_lst[iterator] > upper_limit:
        input_lst.remove()
    else:
        iterator += 1


print(input_lst)

# Завдання2 Є два довільних числа які відповідають за мінімальну і максимальну ціну.
# Є Dict з назвами магазинів і цінами:
# { "citos": 47.999, "BB_studio" 42.999, "mo-no": 49.999, "my-main-service": 37.245,
#   "buy-now": 38.324, "x-store": 37.166, "the-partner": 38.988, "store-123": 37.720, "roze-tka": 38.003}.
#  Напишіть код, який знайде і виведе на екран назви магазинів, ціни яких потрапляють в діапазон між мінімальною і максимальною ціною.
# Наприклад: # lower_limit = 35.9 # upper_limit = 37.339
#
# > match: "x-store", "main-service"

# задайте Dict
input_dict = { "citos": 47.999, "BB_studio": 42.999, "mo-no": 49.999, "my-main-service": 37.245,
               "buy-now": 38.324, "x-store": 37.166, "the-partner": 38.988, "store-123": 37.720, "roze-tka": 38.003}
# задайте межі для пошуку по ціні
lower_limit = 35.9
upper_limit = 37.339

match_counter = 0
print("> match: ")
for key in input_dict.keys():
    if lower_limit <= input_dict[key] <= upper_limit:
        print(key)
        match_counter += 1

try:
    1 / match_counter
except ZeroDivisionError:
    print("No match for selected price range!")

