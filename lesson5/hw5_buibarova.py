# є url https://dummyjson.com/users (100 сторінок)
# вивести в консоль середній вік чоловіків з Brown волоссям,
# а також сформувати список людей, що проживають в місті Louisville
# обовязково прикріпити requirements.txt


import requests

url = "https://dummyjson.com/users?limit=100"

response = requests.get(url)
users_json = response.json()
users = users_json["users"]

total_age = 0
brown_haired_counter = 0
louisville_citizens = []

for user in users:
    gender = user.get("gender", "")
    hair_color = user.get("hair", "").get("color", "")

    if gender.lower() == "male" and hair_color.lower() == "brown":
        user_age = user.get("age", 0)
        if user_age != 0:
            brown_haired_counter += 1
        total_age += user_age

    city = user.get("address", "").get("city", "")

    if city.lower() == "louisville":
        first_name = user.get("firstName", "Unknown")
        last_name = user.get("lastName", "Unknown")
        name = first_name + " " + last_name
        louisville_citizens.append(name)

try:
    average_age = round((total_age / brown_haired_counter), 2)
    print(f"Cередній вік чоловіків з Brown волоссям: {average_age}")
except ZeroDivisionError:
    print("No brown-haired males were found!")

print(f"Cписок мешканців міста Louisville: {louisville_citizens}")



pass
