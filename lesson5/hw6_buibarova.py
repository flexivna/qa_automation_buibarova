# створити АРІ на базі гугл таблиці, містить поля "назва товару", "опис товару", "ціна" (інтова чи флоат),
# "залишок" (інтовий чи флоат), "містить глютен" (булеве тру чи фолс (виставляєте прапорцем).
# заповнити мінімум 10 позицій. дані мають бути отримати по зовнішньому ключу "goods" (not "data")
# за допомогою requests завантажити створені дані. порахувати вартість всіх товарів та товарів без глютена.

import requests

url = "https://script.google.com/macros/s/AKfycbx9ikeCT8nNoN5Qcky6w8yry6Zg1Ja0InCLMhM0s6WMIa_yTqFxbM_FXyUPhwnohqjrQg/exec"
response = requests.get(url)
goods_json = response.json()

goods = goods_json["goods"]
total_price = 0
gluten_free_price = 0


for item in goods:
    gluten_free = not item.get("Містить Глютен", True)
    item_price = item.get("Ціна, грн", 0)
    item_in_stock = item.get("Залишок", 0)
    item_in_stock_price = item_price * item_in_stock
    total_price += item_in_stock_price

    if gluten_free:
        gluten_free_price += item_in_stock_price

print(f"вартість всіх товарів: {total_price}, UAH")
print(f"вартість всіх товарів без глютена: {gluten_free_price}, UAH")



