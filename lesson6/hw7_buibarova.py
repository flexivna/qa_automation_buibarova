# Завдання 1
# Напишіть функцію, яка приймає два аргументи.
# Якщо обидва аргумени відносяться до числових типів функція пермножує ці аргументи і повертає результат
# Якшо обидва аргументи відносяться до типу стрінг функція обʼєднує їх в один і повертає
# В будь-якому іншому випадку - функція повертає кортеж з двох агрументів

def multiply_or_concat(arg1, arg2):
    if type(arg1) == type(arg2) == str:
        result = arg1 + arg2
        return result
    elif (type(arg1) == int or type(arg1) == float) and (type(arg2) == int or type(arg2) == float):
        result = arg1 * arg2
        return result
    else:
        result = (arg1, arg2)
        return result


arg1 = "test"
arg2 = 3

print(multiply_or_concat(arg1, arg2))

# Завдання 2:
# Візьміть код з заняття і доопрацюйте натупним чином:
# користувач має вгадати чизло за певну кількість спроб. користувач на початку програми визначає кількість спроб
# додайте підказки. якщо рвзнися між числами більше 10 - "холодно", від 10 до 5 - "тепло", 1-4 - "гаряче"

from random import randint


def get_ai_number():
    number = randint(1, 10)
    print(f'AI: {number}')
    return number


# нова функція get_tries() - приймає від гравця кількість спроб. кількість спроб має бути позитивним числом (більше 0)
def get_tries():
    while True:
        try:
            result = int(input('How many tries do you need? Please, enter: '))
        except ValueError:
            print('Number please!')
            continue
        if result > 0:
            return result
        else:
            print('Only positive numbers please!')


def get_user_number():
    while True:
        try:
            return int(input('Enter the number (int): '))
        except ValueError:
            print('Number please!')


def check_numbers(ai_number, user_number):
    result = ai_number == user_number
    print(f'Result is: {result}')
    return result


# нова функція get_hint(ai_number, user_number) - додає підказки.
#  якщо різниця між числами більше 10 - "It's cold!", від 10 до 5 - "Getting warmer!", 1-4 - "It's hot!"
def get_hint(ai_number, user_number):
    distance = abs(ai_number - user_number)
    if distance < 5:
        result = "It's hot!"
    elif distance < 10:
        result = "Getting warmer!"
    else:
        result = "It's cold!"
    return result


# Змінена основна функція - додано обмеження по кількості спроб і підказки
def game_guess_number():
    print('Game begins!')

    ai_number = get_ai_number()
    countdown = get_tries()

    while countdown > 0:
        print(f'Tries left: {countdown}')
        user_number = get_user_number()
        is_game_end = check_numbers(ai_number, user_number)

        if is_game_end:
            print('User win')
            break

        countdown -= 1

        if countdown == 0:
            print('User lose')
        else:
            hint = get_hint(ai_number=ai_number, user_number=user_number)
            print(f'{hint} Try again!')


game_guess_number()
