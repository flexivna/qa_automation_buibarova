# Functions
#
# def function_name(arg1, arg2):
#     print(arg1)
#     print(arg1)
#     res = aeg1 + arg2
#     return res
#
#
# result = function_name(1, 2)
# print(result)
#
# # single responsibility principle SRP (SOLID)
#  # optional arguments
#
#  def custom_function_add (a=0, b = 0, c = 0, d= None):
#      if d is None:
#          return
#      print(f"a is {a}")
#      print(f"b is {b}")
#      print(f"c is {c}")

from random import randint


def get_ai_number():
    number = randint(1, 10)
    print(f"AI: {number}")
    return number


def get_user_number():
    while True:
        try:
            return int(input("Enter the number: "))
        except ValueError:
            print("Number please!")


def check_numbers(ai_number, user_number):
    result = ai_number == user_number
    print(f'Result is: {result}')
    return result


def game_guess_number():
    print("Game on!")
    ai_number = get_ai_number()
    while True:
        user_number = get_user_number()
        is_game_end = check_numbers(ai_number, user_number)

        if is_game_end:
            break
        print("Wrong, guess again!")

    print("You win!")


game_guess_number()
