# def my_function(arg1, arg2=10, *args, **kwargs):
#     print(arg1, arg2, args, kwargs)
#     return
#
# my_function()

value = 101094949


# def my_function():
#     print(value)
#
#
# my_function()
#
# print(dir())
#
#
# # docstring
#
#
# def my_custom_add(arg1, arg2):
#     """function for add a to b"""
#     return arg1 + arg2
#
# #restructured text
# def another_custom_func(arg1, arg2):
#     """"
#
#     """

# useful functions
def foo(val):
    return str(val % 3) + 'a'


data = (3, 4, 5)

mapped = map(foo, data)

for i in mapped:
    print(i)


for i in map(foo, data):
    print(i)

