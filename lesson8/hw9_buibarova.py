# Напишіть декоратор, який визначає час виконання функції. Заміряйте час виконання функцій з попереднього ДЗ
# Візьміть функції з попереднього ДЗ, покладіть їх у файл lib.py і імпортуйте в основний файл для виконання


import time
from lib import dumb_calculator
from lib import get_season


def get_execution_time(func_to_measure):
    """"
    A decorator that measures the execution time of a function.

    Args:
        func_to_measure (callable): The function to be measured.

    Returns:
        callable: The wrapper.
    """
    def _wrapper(*args, **kwargs):
        repeats = 10000
        start_time = time.time()
        for i in range(1, repeats):
            result = func_to_measure(*args, **kwargs)
        end_time = time.time()
        duration = (end_time - start_time)/(repeats - 1)
        print(f"Duration of {func_to_measure.__name__} function execution was: {duration} s")
        return result

    return _wrapper


# applying decorator to functions from previous HW
decorated_calculator = get_execution_time(dumb_calculator)
decorated_season_getter = get_execution_time(get_season)

# executing functions from previous HW
print(decorated_calculator(36, 49, "+"))
print(decorated_season_getter("13.04"))

pass
