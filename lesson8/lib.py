# library which contains functions from HW8

def get_season(date_string):
    """
     Returns the season (winter, spring, summer, or autumn) based on given date.

    Args:
        date_string (str): A string in format [day].[month], e.g 12.01

    Returns:
        str: Name of the season, or specific message if season can't be identified
    """
    try:
        day, month = map(int, date_string.split('.'))
    except ValueError:
        return "Incorrect date format!"

    if month == 12 or month <= 2:
        return "Winter"
    elif 3 <= month <= 5:
        return "Spring"
    elif 6 <= month <= 8:
        return "Summer"
    elif 9 <= month <= 11:
        return "Autumn"
    else:
        return "There is no such month!"


def dumb_calculator(arg1, arg2, operation):
    """
    Simple calculator, which supports two numeric arguments and following operations: + - / *

    Args:
        arg1 (int or float): first numeric argument
        arg2 (int or float): second numeric argument
        operation (str): arithmetic operation which will be performed. Must be one of + - / *

    Returns:
        int or float or None. Returns either numeric result of arithmetic operation or None and prints error message

    """
    if not (isinstance(arg1, (int, float)) and isinstance(arg2, (int, float))):
        print("Incorrect arguments type!")
        return None
    if operation == "+":
        return arg1 + arg2
    elif operation == "-":
        return arg1 - arg2
    elif operation == "*":
        return arg1 * arg2
    elif operation == "/":
        try:
            return arg1 / arg2
        except ZeroDivisionError:
            print("Can't divide by zero")
            return None
    else:
        print("Operation isn't supported!")
        return None
